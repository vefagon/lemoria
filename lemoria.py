# lemoria.py - a minimal flask api using flask_restful
import os
import requests
from flask import Flask, jsonify,redirect
from flask import make_response
from healthcheck import HealthCheck

app = Flask(__name__)

# wrap the flask app and give a heathcheck url
health = HealthCheck(app, "/healthcheck")



lemoriapp = [
    {
        'version': u'1.0',
        'description': u'VARDESC', 
        'lastcommitbranch': u'VARCOMBRANCH',
        'lastcommitsha': u'VARCOMSHA'
    }
]

@app.route('/about', methods=['GET'])
def about_info():
    response = jsonify({'lemoriapp': lemoriapp})
    response.status_code = 200
    return response

@app.route('/') # rootEnpoint Hello Universe
def hello_universe():
    response = jsonify({'Aloha': 'Universo'})
    response.status_code = 200
    return response

@app.route('/ping', methods=['GET'])
def web_available():
    response = jsonify({'ping': u"pong"})
    return True, "Web Server Running"

health.add_check(web_available)

@app.route("/easter-egg")
def easter_egg():
    return redirect("http://wttr.in/melbourne", code=302)
    #return requests.get('wttr.in/melbourne').content



@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True, host='0.0.0.0', port=port)


