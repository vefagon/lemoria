# Lemoria Contariner RESTfull API 
This is a basic RESTfull API programmed in [Flask](http://flask.pocoo.org/) and [Python](https://www.python.org/) and containerized using [Docker](https://www.docker.com/).

Its main characteristics are is a simple, small, operable web-style API and you can deploy it locally or remotely. In this last case, you will be using a service solution that is running and living in a Cloud Application Platform, [Heroku](https://www.heroku.com/). In addition, you will be able to deploy and trigger automatically its release using the [GitLab](https://about.gitlab.com/) [CI/CD feature](https://docs.gitlab.com/ee/ci/). That means, that you will be 

## Application Details

The API contain four main feaures that will return the data in [json](https://www.json.org/) format:

1) A simple root endpoint which responds "Aloha": "Miloha Universo" 
2) A health endpoint about information of the availability of the web service
3) A metadata endpoint which returns basic information about the application
4) Also, you will be able to run a basic unit test to check if the main page is serving properly

## How it works ![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Twemoji2_1f914.svg/32px-Twemoji2_1f914.svg.png)

### Prerequisites

Before you start, you need to install some programs (if they are not living in your computer yet):

If you want to try running it locally:

- [Docker Community Edition](https://docs.docker.com/install/) 
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Python](https://www.python.org/downloads/)
- [Pip](https://pypi.org/project/pip/). The PyPA recommended tool for installing Python packages.
- [Pytest](https://docs.pytest.org/en/latest/getting-started.html)  (This one if optional and it is only necessary if you want to run unit test in the application)
- [Curl](https://curl.haxx.se/)

Or, running it remotly using GitLab CI/CD:

- [Docker Community Edition](https://docs.docker.com/install/) 
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Curl](https://curl.haxx.se/)

### How to deploy and run Lemoria RESTfull API in a local environment

The exercices is focus in the utilization of CLI tools, for instance, the bash terminal can be use in Microsoft Windows and Linux OS. Ones you have a bash terminal sesion started:

a) Clone **Lemoria RESTfull API** from `git` repostitory

```bash
$ git clone https://gitlab.com/vefagon/lemoria.git
Cloning into 'lemoria'...
remote: Enumerating objects: 179, done.
remote: Counting objects: 100% (179/179), done.
remote: Compressing objects: 100% (100/100), done.
remote: Total 179 (delta 96), reused 134 (delta 68)
Receiving objects: 100% (179/179), 18.61 KiB | 18.61 MiB/s, done.
Resolving deltas: 100% (96/96), done.

```

b) Enter to the cloned `git directory -> lemoria` and build the [docker image](https://docs.docker.com/engine/reference/commandline/images/) that is defined in the `Dockerfile`. This file also have the sotware requirements definition for the proper deploy of application.

```bash
$ cd lemoria/
# Validating directory - git repo
$ git remote -v
origin	https://gitlab.com/vefagon/lemoria.git (fetch)
origin	https://gitlab.com/vefagon/lemoria.git (push)

# Checking the existing build images (in my case I do not have any configured yet)
$ docker images
REPOSITORY  TAG IMAGE ID    CREATED SIZE
$ docker container ls
CONTAINER ID    IMAGE   COMMAND CREATED STATUS  PORTS   NAMES

$ docker build -t lemoriapp:latest Dockerfile

docker build -t lemoriapp:latest .
Sending build context to Docker daemon  95.74kB
Step 1/10 : FROM alpine:3.1
 ---> a1038a41fe2b
Step 2/10 : RUN apk add --update python py-pip bash
 ---> Using cache
 ---> 424edc6d4818
Step 3/10 : ENV APP_DIR /app
 ---> Using cache
 ---> 0560fd6fcf39
Step 4/10 : COPY . /app
 ---> 56e4e1ea9807
Step 5/10 : WORKDIR /${APP_DIR}
 ---> Running in fd8b3d33e01c
Removing intermediate container fd8b3d33e01c
 ---> 69912de8ed5b
Step 6/10 : RUN pip install -r requirements.txt
 ---> Running in a1ba016df7ab
Successfully installed flask flask-restful healthcheck requests Jinja2 click Werkzeug itsdangerous pytz six aniso8601 idna urllib3 certifi chardet MarkupSafe
Cleaning up...
Removing intermediate container a1ba016df7ab
 ---> c12d21d79380
Step 7/10 : RUN adduser -D myuser
 ---> Running in 390c159a66e7
Removing intermediate container 390c159a66e7
 ---> 0305e3307850
Step 8/10 : USER myuser
 ---> Running in cb58a5db5c73
Removing intermediate container cb58a5db5c73
 ---> 8c5ffe12a128
Step 9/10 : ENTRYPOINT ["python"]
 ---> Running in 533a2f8436c7
Removing intermediate container 533a2f8436c7
 ---> 7b94bcb3097a
Step 10/10 : CMD ["lemoria.py"]
 ---> Running in 5e5ade1dd4c7
Removing intermediate container 5e5ade1dd4c7
 ---> e7b6f0b3f346
Successfully built e7b6f0b3f346
Successfully tagged lemoriapp:latest

```

c) After the build image intruction, you are able to check the new image:
```bash
$ docker images
REPOSITORY     TAG     IMAGE ID       CREATED        SIZE
lemoriapp      latest  e7b6f0b3f346   4 minutes ago  58.6MB
# But the container it is not ready yet
$ docker container ls
CONTAINER ID    IMAGE   COMMAND CREATED STATUS  PORTS   NAMES

```

d) Now it is time to [run](https://docs.docker.com/engine/reference/run/) and deploy the [docker container](https://stackoverflow.com/questions/23735149/what-is-the-difference-between-a-docker-image-and-a-container) based on your new docker images.

```bash
# The output of the run command it is the container ID
$ docker run -d -p 5000:5000 lemoriapp:latest
bd6700f679f53cd3d3e5a644eee8a8afd5af5b7d3ece5fa7facfe1ae86c591e3
# Let's check the status of the new container
$ docker container ls
CONTAINER ID   IMAGE            COMMAND               CREATED         STATUS         PORTS                  NAMES
bd6700f679f5   lemoriapp:latest "python lemoria.py"   46 seconds ago  Up 45 seconds  0.0.0.0:5000->5000/tcp quizzical_wilbur
```

The above step shows you that the container is nunning and ready to serve you. So let's try it:

1) Checking the root endpoint of the application by `curl`:

```bash
$ curl http://127.0.0.1:5000
{
  "Aloha": "Miloha Universo"
}
```
1.1 Using verbose option

```bash
$ curl -v http://127.0.0.1:5000
* Rebuilt URL to: http://127.0.0.1:5000/
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 5000 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1:5000
> User-Agent: curl/7.52.1
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-Type: application/json
< Content-Length: 33
< Server: Werkzeug/0.15.2 Python/2.7.12
< Date: Mon, 12 May 2019 02:24:55 GMT
< 
{
  "Aloha": "Miloha Universo"
}
* Curl_http_done: called premature == 0
* Closing connection 0
```

2) Checking the health endpoint using `curl`:

```bash
$ curl http://127.0.0.1:5000/healthcheck
{   "status": "success", 
    "timestamp": 1557716220.408403, 
    "hostname": "bd6700f679f5", 
    "results": [
        {"output": "Web Server Running", 
            "checker": "web_available", 
            "expires": 1557716221.73978, 
            "passed": true, 
            "timestamp": 1557716194.73978
        }
        ]
}
```

3) Checking the root endpoint of the application by `curl`:
   
```bash
$ curl http://127.0.0.1:5000/about
{
  "lemoriapp": [
    {
      "description": "re-interview technical test - Veronica Farias", 
      "lastcommitbranch": "master", 
      "lastcommitsha": "9064885508eee27f36bc1794ee7cd7deb346f748", 
      "version": "1.0"
    }
  ]
}
```

4) Running local unit test (this unit test check if the root endpoint is working)

```bash
#Assuming that you are inside the lemoria folder project.
$ pytest test_local/
============ test session starts =======================================
platform linux2 -- Python 2.7.13, pytest-4.4.2, py-1.8.0, pluggy-0.11.0
rootdir: /home/paltitus/myob/lemoria
collected 1 item                                                         of the app in a standa                                                                                  

test_local/test_endpoints.py .                                     [100%]of the app in a standa

============ 1 passed in 0.32 seconds ===================================

```


From here, you can keep your new deployments `up-to-date` based on your new relases, using the advantage of having a version control system like GithHub or GitLab, where you can upload in a easy way the code changes of your application. 

After that you will be able to build, deploy, run and test you application again, but if the only process that has more changes during the time is the code, can you automate the other steps (build, deploy, run and test)?.The answer os "of course mate", and that is the following topic in this activity.

## How to deploy and run Lemoria RESTfull API in a remote environment using GitLab CI/CD

This one is the actual URL service https://lemoria.herokuapp.com/
 that [it is deployed using the same code showed in the previous and running in Heroku](https://devcenter.heroku.com/articles/container-registry-and-runtime). 

Therefore, let's check each endpoint of the application:

 1) Checking the root endpoint of the application by `curl`:

```bash
$ curl  https://lemoria.herokuapp.com/
{
  "Aloha": "Miloha Universo"
}

```
1.1 Using verbose option

```bash
curl -v https://lemoria.herokuapp.com/
*   Trying 52.20.167.39...
* TCP_NODELAY set
* Connected to lemoria.herokuapp.com (52.20.167.39) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* Cipher selection: ALL:!EXPORT:!EXPORT40:!EXPORT56:!aNULL:!LOW:!RC4:@STRENGTH
* successfully set certificate verify locations:
*   CAfile: /etc/ssl/certs/ca-certificates.crt
  CApath: /etc/ssl/certs
* TLSv1.2 (OUT), TLS header, Certificate Status (22):
* TLSv1.2 (OUT), TLS handshake, Client hello (1):
* TLSv1.2 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS change cipher, Client hello (1):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES128-GCM-SHA256
* ALPN, server did not agree to a protocol
* Server certificate:
*  subject: C=US; ST=California; L=San Francisco; O=Heroku, Inc.; CN=*.herokuapp.com
*  start date: Apr 19 00:00:00 2017 GMT
*  expire date: Jun 22 12:00:00 2020 GMT
*  subjectAltName: host "lemoria.herokuapp.com" matched cert's "*.herokuapp.com"
*  issuer: C=US; O=DigiCert Inc; OU=www.digicert.com; CN=DigiCert SHA2 High Assurance Server CA
*  SSL certificate verify ok.
> GET / HTTP/1.1
> Host: lemoria.herokuapp.com
> User-Agent: curl/7.52.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Connection: keep-alive
< Content-Type: application/json
< Content-Length: 33
< Server: Werkzeug/0.15.2 Python/2.7.12
< Date: Mon, 13 May 2019 03:39:28 GMT
< Via: 1.1 vegur
< 
{
  "Aloha": "Miloha Universo"
}
* Curl_http_done: called premature == 0
* Connection #0 to host lemoria.herokuapp.com left intact
```

2) Checking the health endpoint using `curl`:

```bash
$ curl https://lemoria.herokuapp.com/healthcheck
{   "status": "success", 
    "timestamp": 1557718808.550468, 
    "hostname": "3cbed0d1-2aaa-45bf-b7ec-53c5ef7af73f", 
    "results": [
        {"output": "Web Server Running", 
            "checker": "web_available", 
            "expires": 1557718835.55044, 
            "passed": true, 
            "timestamp": 1557718808.55044
        }
        ]
}
```

3) Checking the root endpoint of the application by `curl`:
   
```bash
$ curl https://lemoria.herokuapp.com/about
{
  "lemoriapp": [
    {
      "description": "re-interview technical test - Veronica Farias", 
      "lastcommitbranch": "master", 
      "lastcommitsha": "e58b9ae78e2de0de0617d0b0069883880abea27f", 
      "version": "1.0"
    }
  ]
}
```

After taking a look of the API, let's try the next stage that is to use the pipeline to buil, deploy, run and test it using GitLab CI/CD:

a) Clone **Lemoria RESTfull API** from `GitLab` repostitory, this time we will use SSH authentication to interact with GitLab.

```bash
ssh-agent $(ssh-add /home/$USER/.ssh/id_rsa_gd; git clone git@gitlab.com:vefagon/lemoria.git)
Identity added: /home/USER/.ssh/id_rsa_gd (GitLab-Deploy)
Cloning into 'lemoria'...
remote: Enumerating objects: 179, done.
remote: Counting objects: 100% (179/179), done.
remote: Compressing objects: 100% (100/100), done.
remote: Total 179 (delta 96), reused 134 (delta 68)
Receiving objects: 100% (179/179), 18.61 KiB | 2.66 MiB/s, done.
Resolving deltas: 100% (96/96), done.
SSH_AUTH_SOCK=/tmp/ssh-LY7yTiwu3SLW/agent.1483; export SSH_AUTH_SOCK;
SSH_AGENT_PID=1484; export SSH_AGENT_PID;
echo Agent pid 1484;
```

b) Change some code to commit and trigger the CI/CD process

From: 
```bash
 30 @app.route('/') # rootEnpoint Hello Universe
 31 def hello_universe():
 32     response = jsonify({'Aloha': 'Miloha Universo'})
 33     response.status_code = 200
 34     return response
```

 To
```bash
  30 @app.route('/') # rootEnpoint Hello Universe
 31 def hello_universe():
 32     response = jsonify({'Aloha': 'Universo'})
 33     response.status_code = 200
 34     return response
```

Check the code changes:

```bash
git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   lemoria.py

no changes added to commit (use "git add" and/or "git commit -a")
```

```bash
$ git add lemoria.py
$ git commit -m "New release of lemonia api Aloha Universo"
$ git push origin master
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 370 bytes | 370.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To gitlab.com:vefagon/lemoria.git
   7249713..8e12fff  master -> master
```

After the commit, the CI/CD pipeline is going to start the Build, Deloy and Test processes automatically as you can see in the following image:

**GitLab CI/CD pipeline**
Lemoria Projecy URL: https://gitlab.com/vefagon/lemoria/pipelines

![alt text](https://gitlab.com/vefagon/lemoria/raw/master/images/GitLabCICD-Pipeline.png)

**GitLab CI/CD pipeline Jobs definition**
![alt text](https://gitlab.com/vefagon/lemoria/raw/master/images/GitLabCICD-Pipeline-Jobs.png)

If you want to check the definition of that pipeline, then you can have a look of this file [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/):

```bash
image: docker:latest
services:
  - docker:dind

stages:
  - build
  - deploy
  - test

variables:
  DOCKER_DRIVER: overlay
  HEROKU_REG: "registry.heroku.com"
  CONTAINER_TEST_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}_${CI_COMMIT_SHA}_test
  CONTAINER_DEPLOY_IMAGE: ${HEROKU_REG}/${CI_PROJECT_NAME}/web
  HEROKU_API_KEY: ${HEROKU_AUTH_TOKEN}
  HEROKU_USER: "vefagon@outlook.com"
  DOCKER_REPO: "hub.docker.com"
  DOCKER_IMAGE: "verogo/lemoria"
  USERNAME: "verogo"
  DESCRIPTION: "re-interview technical test - "
  OWNER: "Veronica Farias"

before_script:
  - apk add --update add --update sed python py-pip bash curl pytest && rm -rf /var/cache/apk/*
  
docker-build:
  stage: build
  script:
    - echo ${CI_COMMIT_REF_NAME}
    - sed -i "s/VARCOMBRANCH/${CI_COMMIT_REF_NAME}/g" "lemoria.py"
    - sed -i "s/VARCOMSHA/${CI_COMMIT_SHA}/g" "lemoria.py"
    - sed -i "s/VARDESC/${DESCRIPTION}${OWNER}/g" "lemoria.py"
    - docker login -u _ -p $HEROKU_AUTH_TOKEN registry.heroku.com
    - docker build --build-arg NODE=development --pull -t $CONTAINER_TEST_IMAGE .

docker-deploy:
  stage: deploy
  script:
    - docker login -u _ -p $HEROKU_AUTH_TOKEN registry.heroku.com
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_DEPLOY_IMAGE
    - docker push $CONTAINER_DEPLOY_IMAGE
    - chmod +x ./release.sh
    - ./release.sh $CI_PROJECT_NAME

docker-test:
  stage: test
  script:
    - pip install pytest
    - pip install requests
    - pytest ./test/
```


Check the new app release:

Root endpoint:

```bash
$ curl https://lemoria.herokuapp.com/
{
  "Aloha": "Universo"
}
```

```bash
$ curl https://lemoria.herokuapp.com/about
{
  "lemoriapp": [
    {
      "description": "re-interview technical test - Veronica Farias", 
      "lastcommitbranch": "master", 
      "lastcommitsha": "8e12fffd40afa41bd927192e8395400350f35de2", 
      "version": "1.0"
    }
  ]
}
```

```bash
$ curl https://lemoria.herokuapp.com/healthcheck

{   "status": "success", 
    "timestamp": 1557721174.97924, 
    "hostname": "1b69853e-2a58-4122-b692-77e2c90da12b", 
    "results": [
        {"output": "Web Server Running", 
            "checker": "web_available", 
            "expires": 1557721201.979222, 
            "passed": true, 
            "timestamp": 11557721201.979222
        }
        ]
}
```
 


So that is all mates (for now)!

![alt text](https://66.media.tumblr.com/f5436f265630043b4163b3b796436229/tumblr_nerv5zwzCr1sgl0ajo1_500.gif)

**Note:** Keep in mind that this API has been deployed with a very basic security standard (for the moment). 

Although if you are using it in remote mode, it is served in a secure protocol (HTTPS), it does not achieve all the security standards that should have any API. 

For instance, it does not contain any [Authentication and Authorization](https://howtodoinjava.com/resteasy/jax-rs-resteasy-basic-authentication-and-authorization-tutorial/) implemented either other [Security Essentials](https://restfulapi.net/security-essentials/) standards. Therefore, it is not recommended deploying it in production environments until these feautures will be ready. 
