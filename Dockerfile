FROM alpine:3.1

#Update software package python and Flask
RUN apk add --update python py-pip bash


# Application Folder Name Environment Variable
ENV APP_DIR /app

#Copy the files from local to container
COPY . /app

#Definition of the work directory of the application

WORKDIR /${APP_DIR}

RUN pip install -r requirements.txt

RUN adduser -D myuser
USER myuser

ENTRYPOINT ["python"]

CMD ["lemoria.py"]

